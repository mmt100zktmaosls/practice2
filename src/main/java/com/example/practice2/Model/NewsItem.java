package com.example.practice2.Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsItem {
    private String title;
    private String content;
}
