package com.example.practice2.Controller;

import com.example.practice2.Model.NewsItem;
import com.example.practice2.Service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<NewsItem> getHtml() throws IOException {
        List<NewsItem> result = scrapService.run();
        return result;
    }
}
